var app = {
	// Application Constructor
	initialize: function () {
		document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
		document.addEventListener('deviceready', function () {
			// Enable to debug issues.
			// window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});

			var notificationOpenedCallback = function (jsonData) {
				console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
			};

			window.plugins.OneSignal
				.startInit("d2e1ce8a-39e6-44da-9d8b-cc22e4fc65f2")
				.handleNotificationOpened(notificationOpenedCallback)
				.endInit();

			window.plugins.OneSignal.getIds(function (ids) {
				console.log('getIds: ' + JSON.stringify(ids));
				console.log("userId = " + ids.userId + ", pushToken = " + ids.pushToken);
				// alert("userId = " + ids.userId + ", pushToken = " + ids.pushToken);

				document.getElementById("OneSignalUserID").innerHTML = ids.userId;
				document.getElementById("OneSignalPushToken").innerHTML = ids.pushToken;
			});

			// Call syncHashedEmail anywhere in your app if you have the user's email.
			// This improves the effectiveness of OneSignal's "best-time" notification scheduling feature.
			// window.plugins.OneSignal.syncHashedEmail(userEmail);
		}, false);

		// adciona o onclick nos botoes
		document.getElementById("btnContinuar").addEventListener("click", iniciaVerificacao, false);
		document.getElementById("btnContinuar").addEventListener("touchstart", iniciaVerificacao, false);

		// inicia a funcao de verificacao
		//setTimeout(verificaProntidao, 2000);

		// funcao para avisar que deu o tempo maximo para carregar
		//setTimeout(function(){
		//	tempoEsgotado = true;
		//}, 10000);

	},

	// deviceready Event Handler
	//
	// Bind any cordova events here. Common events are:
	// 'pause', 'resume', etc.
	onDeviceReady: function () {
		console.log('device ready');

		this.receivedEvent('deviceready');

//		document.addEventListener("backbutton", onBackKeyDown, false);

//		function onBackKeyDown(e) {
//			e.preventDefault();
//		}
	},

	// Update DOM on a Received Event
	receivedEvent: function (id) {
		console.log('Received Event: ' + id);
	},
};

function onError(error) {
	alert("code: " + error.code +
		"message:" + error.message);
}

function abrirApp() {
	// alert('CLICOU!')
	var pushID = document.getElementById("OneSignalUserID").textContent;
	var pushToken = document.getElementById("OneSignalPushToken").textContent;
	var urlRedirect = 'http://gourmex.com/?';
	var urlRedirectFinal = urlRedirect + "&isApp=true" + "&appUserId=" + pushID + "&appPushToken=" + pushToken;
	// alert(`PushID = ${pushID} , PushToken = ${pushToken} , ${urlRedirectFinal}`);

	// volta funcao dos botoes
	document.getElementById("btnContinuar").addEventListener("click", iniciaVerificacao, false);
	document.getElementById("btnContinuar").addEventListener("touchstart", iniciaVerificacao, false);

	// esconde o loader pequeno
	document.getElementById("carregando").style.display = 'none';

	window.open(urlRedirectFinal, "_self", 'location=no,toolbar=no,zoom=no,hardwareback=no');
}

// variavel para informar se o tempo para carregar foi esgotado, para nao ficar carregando permanentemente
tempoEsgotado = false;

function iniciaVerificacao() {
	// mostra botao apos abrir o app em caso de voltar a tela inicial
	//document.getElementById("divBotaoContinuar").style.display = 'none'
	document.getElementById("carregando").style.display = 'block';

	// retira funcao dos botoes
	document.getElementById("btnContinuar").removeEventListener("click", iniciaVerificacao, false);
	document.getElementById("btnContinuar").removeEventListener("touchstart", iniciaVerificacao, false);

	// inicia a funcao de verificacao
	setTimeout(verificaProntidao, 1000);

	// inicia o timer para nao ficar tentando pegar as informacoes permanentemente
	setTimeout(function(){
		tempoEsgotado = true;
	}, 5000);
}

function verificaProntidao() {
	//variavel para verificar se existe conexao com a internet
	var online = navigator.onLine;

	if (online) {
		var userid = document.getElementById("OneSignalUserID").innerHTML
		var pushtoken = document.getElementById("OneSignalPushToken").innerHTML
		if (userid.length > 0 && pushtoken.length > 0) {
			abrirApp();
		} else if (tempoEsgotado == false) {
			setTimeout(verificaProntidao, 1000);
		} else {
			abrirApp();
		}
	} else {
		document.getElementById("carregando").style.display = 'none';
		document.getElementById("naoCarregou").style.display = 'block';
	}
}

app.initialize();